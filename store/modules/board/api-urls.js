const BASE_URL = '/v1/board'

export default {
    DO_NOTICE_LIST: `${BASE_URL}/notice/all`, //get 공지사항만 조회
    DO_GUIDE_LIST: `${BASE_URL}/use/all`, //get 이용방법만 조회
    DO_NOTICE_DETAIL: `${BASE_URL}/detail/notice/{id}`, //get 공지사항 상세조회
    DO_CREATE_NOTICE: `${BASE_URL}/new/member-id/{memberId}`, //post 공지사항 등록
}
