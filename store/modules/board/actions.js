import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_NOTICE_LIST]: (store) => {
        return axios.get(apiUrls.DO_NOTICE_LIST)
    },
    [Constants.DO_GUIDE_LIST]: (store) => {
        return axios.get(apiUrls.DO_GUIDE_LIST)
    },
    [Constants.DO_NOTICE_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_NOTICE_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_CREATE_NOTICE]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE_NOTICE.replace('{memberId}', payload.id), payload.data)
    },
}
